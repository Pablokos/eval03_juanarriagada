<%-- 
    Document   : index
    Created on : 03-07-2021, 22:49:38
    Author     : jparriagada
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 3 - Ciisa</title>
    </head>
    <body>
        <h2>EndPoints Api Persona Natural</h2>
        <br>
        <br>
        <br>
        <h2>Lista de Personas Naturales - GET ----------------------------------------------> <FONT COLOR="red">https://eval03-juanarriagada.herokuapp.com/api/persona_natural</FONT></h2>
        <br>
        <br>
        <br>
        <h2>Lista de Personas Naturales por RUT - GET ----------------------------------> <FONT COLOR="red">https://eval03-juanarriagada.herokuapp.com/api/persona_natural/16076495</FONT></h2>
        <br>
        <br>
        <br>
        <h2>Crear Personas Naturales - POST -----------------------------------------------> <FONT COLOR="red">https://eval03-juanarriagada.herokuapp.com/api/persona_natural</FONT></h2>
    ejemplo
        <br>
        <br>
    {
        "apellidoMaterno": "",
        "apellidoPaterno": "Cruces",
        "dv": "5",
        "nombre": "Cesar",
        "rut": 55555555
    }
        <br>
        <br>
        <br>
        <h2>Actualizar Personas Naturales - PUT -------------------------------------------> <FONT COLOR="red">https://eval03-juanarriagada.herokuapp.com/api/persona_natural</FONT></h2>
        ejemplo
        <br>
        <br>
    {
       "apellidoMaterno": "Arriagada",
        "apellidoPaterno": "Cordovez",
        "dv": "3",
        "nombre": "Juan Pablo",
        "rut": 16076495
    }
        <br>
        <br>
        <br>
        <h2>Eliminar Personas Naturales - DELETE ---------------------------------------> <FONT COLOR="red">https://eval03-juanarriagada.herokuapp.com/api/persona_natural/22222222</FONT></h2>
        <br>
        <br>
        <br>
    </body>
</html>
