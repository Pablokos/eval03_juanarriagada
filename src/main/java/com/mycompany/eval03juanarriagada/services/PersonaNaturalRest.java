/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eval03juanarriagada.services;

import com.mycompany.eval03juanarriagada.dao.PersonaNaturalJpaController;
import com.mycompany.eval03juanarriagada.dao.exceptions.NonexistentEntityException;
import com.mycompany.eval03juanarriagada.entity.PersonaNatural;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jparriagada
 */
@Path("persona_natural")
public class PersonaNaturalRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPersonas() {

        PersonaNaturalJpaController dao = new PersonaNaturalJpaController();

        List<PersonaNatural> persona_natural = dao.findPersonaNaturalEntities();

        return Response.ok(200).entity(persona_natural).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(PersonaNatural persona_natural) {

        PersonaNaturalJpaController dao = new PersonaNaturalJpaController();
        try {
            dao.create(persona_natural);
        } catch (Exception ex) {
            Logger.getLogger(PersonaNaturalRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(persona_natural).build();

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(PersonaNatural persona_natural) {

        PersonaNaturalJpaController dao = new PersonaNaturalJpaController();

        try {
            dao.edit(persona_natural);
        } catch (Exception ex) {
            Logger.getLogger(PersonaNaturalRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(persona_natural).build();

    }

    
    
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") Long ideliminar) {

        PersonaNaturalJpaController dao = new PersonaNaturalJpaController();

        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PersonaNaturalRest.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       return Response.ok("Cliente Eliminado").build();
    }

    
    @GET
    @Path("/{idConsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorRut(@PathParam("idConsultar") Long idConsultar) {
        
        PersonaNaturalJpaController dao = new PersonaNaturalJpaController();
         
        PersonaNatural persona_natural = dao.findPersonaNatural(idConsultar);
        
        return Response.ok(200).entity(persona_natural).build();

    }
    
}
