/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.eval03juanarriagada.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jparriagada
 */
@Entity
@Table(name = "persona_natural")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersonaNatural.findAll", query = "SELECT p FROM PersonaNatural p"),
    @NamedQuery(name = "PersonaNatural.findByRut", query = "SELECT p FROM PersonaNatural p WHERE p.rut = :rut"),
    @NamedQuery(name = "PersonaNatural.findByDv", query = "SELECT p FROM PersonaNatural p WHERE p.dv = :dv"),
    @NamedQuery(name = "PersonaNatural.findByNombre", query = "SELECT p FROM PersonaNatural p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "PersonaNatural.findByApellidoPaterno", query = "SELECT p FROM PersonaNatural p WHERE p.apellidoPaterno = :apellidoPaterno"),
    @NamedQuery(name = "PersonaNatural.findByApellidoMaterno", query = "SELECT p FROM PersonaNatural p WHERE p.apellidoMaterno = :apellidoMaterno")})
public class PersonaNatural implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rut")
    private Long rut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dv")
    private Character dv;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido_paterno")
    private String apellidoPaterno;
    @Size(max = 2147483647)
    @Column(name = "apellido_materno")
    private String apellidoMaterno;

    public PersonaNatural() {
    }

    public PersonaNatural(Long rut) {
        this.rut = rut;
    }

    public PersonaNatural(Long rut, Character dv) {
        this.rut = rut;
        this.dv = dv;
    }

    public Long getRut() {
        return rut;
    }

    public void setRut(Long rut) {
        this.rut = rut;
    }

    public Character getDv() {
        return dv;
    }

    public void setDv(Character dv) {
        this.dv = dv;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonaNatural)) {
            return false;
        }
        PersonaNatural other = (PersonaNatural) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.eval03juanarriagada.entity.PersonaNatural[ rut=" + rut + " ]";
    }
    
}